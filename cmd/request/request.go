package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"time"
)

type post struct {
	contentType string
	Data        Data
}

type Data struct {
	Body  string `json:"body"`
	Title string `json:"title"`
}

func main() {
	p := &post{contentType: "application/json"}
	res, err := p.executeHttpRequest(http.MethodGet, "https://jsonplaceholder.typicode.com/posts/1", nil)
	if err != nil {
		log.Println(err)
	}
	body := bytes.NewReader(res)
	res, err = p.executeHttpRequest(http.MethodPost, "https://postman-echo.com/post", body)
	if err != nil {
		log.Println(err)
	}
	if err := json.Unmarshal(res, &p); err != nil {
		log.Println(err)
	}
	fmt.Printf("Title: %s\nBody: %s\n", string(p.Data.Title), string(p.Data.Body))
}

func (p *post) executeHttpRequest(method, url string, body io.Reader) ([]byte, error) {
	req, err := http.NewRequest(method, url, body)
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", p.contentType)
	client := http.Client{
		Timeout: 30 * time.Second,
	}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	res, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	return res, nil
}
