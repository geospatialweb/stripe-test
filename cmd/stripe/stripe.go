package main

import (
	"fmt"
	"log"
	"os/exec"
	"runtime"

	"github.com/stripe/stripe-go/v74"
	"github.com/stripe/stripe-go/v74/card"
	"github.com/stripe/stripe-go/v74/charge"
	"github.com/stripe/stripe-go/v74/customer"
	"github.com/stripe/stripe-go/v74/paymentlink"
	"github.com/stripe/stripe-go/v74/paymentmethod"
	"github.com/stripe/stripe-go/v74/price"
	"github.com/stripe/stripe-go/v74/product"
	"github.com/stripe/stripe-go/v74/token"
	"gitlab.com/geospatialweb/stripe-test/pkg/env"
)

func main() {
	envFile := ".env"
	env, err := env.LoadEnv(envFile)
	if err != nil {
		log.Fatal(err)
	}
	stripe.Key = env.StripeSecretKey

	cust, err := createCustomer()
	if err != nil {
		log.Println(err)
		return
	}

	sub, err := createSubscriptionProduct()
	if err != nil {
		log.Println(err)
		return
	}

	price, err := createSubscriptionPrice(sub.ID)
	if err != nil {
		log.Println(err)
		return
	}

	if err := createPaymentMethod(cust.ID); err != nil {
		log.Println(err)
		return
	}

	if err := createPaymentCard(cust.ID); err != nil {
		log.Println(err)
		return
	}

	if err := createTestCharge(cust.ID); err != nil {
		log.Println(err)
		return
	}

	if err := createPaymentLink(price.ID); err != nil {
		log.Println(err)
	}
}

func createCustomer() (*stripe.Customer, error) {
	params := &stripe.CustomerParams{
		Address: &stripe.AddressParams{
			Line1:      stripe.String("200 Lancaster Street"),
			Line2:      stripe.String("621"),
			City:       stripe.String("Kitchener"),
			State:      stripe.String("Ontario"),
			PostalCode: stripe.String("N2H 1C6"),
			Country:    stripe.String("CA"),
		},
		Email: stripe.String("johncampbell@geospatialweb.ca"),
		Name:  stripe.String("John Campbell"),
		Phone: stripe.String("15196425871"),
		Shipping: &stripe.CustomerShippingParams{
			Address: &stripe.AddressParams{
				Line1:      stripe.String("200 Lancaster Street"),
				Line2:      stripe.String("621"),
				City:       stripe.String("Kitchener"),
				State:      stripe.String("Ontario"),
				PostalCode: stripe.String("N2H 1C6"),
				Country:    stripe.String("CA"),
			},
			Name:  stripe.String("John Campbell"),
			Phone: stripe.String("15196425871"),
		},
		TaxExempt: stripe.String("none"),
	}
	cust, err := customer.New(params)
	if err != nil {
		return nil, err
	}
	fmt.Printf("Customer ID: %s\n", cust.ID)
	cust, err = customer.Get(cust.ID, nil)
	if err != nil {
		return nil, err
	}
	fmt.Printf("Customer Name: %s\n", cust.Name)
	return cust, nil
}

func createSubscriptionProduct() (*stripe.Product, error) {
	params := &stripe.ProductParams{
		Name:        stripe.String("Subscription Product"),
		Description: stripe.String("$10/Month subscription"),
	}
	sub, err := product.New(params)
	if err != nil {
		return nil, err
	}
	fmt.Printf("Subscription product ID: %s\n", sub.ID)
	return sub, nil
}

func createSubscriptionPrice(subID string) (*stripe.Price, error) {
	params := &stripe.PriceParams{
		Currency: stripe.String(string(stripe.CurrencyUSD)),
		Product:  stripe.String(subID),
		Recurring: &stripe.PriceRecurringParams{
			Interval: stripe.String(string(stripe.PriceRecurringIntervalMonth)),
		},
		TaxBehavior: stripe.String("exclusive"),
		UnitAmount:  stripe.Int64(1000),
	}
	price, err := price.New(params)
	if err != nil {
		return nil, err
	}
	fmt.Printf("Subscription price ID: %s\n", price.ID)
	return price, nil
}

func createPaymentMethod(custID string) error {
	paymentMethodParams := &stripe.PaymentMethodParams{
		Card: &stripe.PaymentMethodCardParams{
			Number:   stripe.String("4242424242424242"),
			ExpMonth: stripe.Int64(3),
			ExpYear:  stripe.Int64(2025),
			CVC:      stripe.String("836"),
		},
		Type: stripe.String("card"),
	}
	paymentMethod, err := paymentmethod.New(paymentMethodParams)
	if err != nil {
		return err
	}
	fmt.Printf("Payment ID : %s\n", fmt.Sprint(paymentMethod.ID))

	paymentMethodAttachParams := &stripe.PaymentMethodAttachParams{
		Customer: stripe.String(custID),
	}
	pm, err := paymentmethod.Attach(
		paymentMethod.ID,
		paymentMethodAttachParams,
	)
	if err != nil {
		return err
	}
	fmt.Printf("Payment method type: %s\n", pm.Type)

	paymentMethodListParams := &stripe.PaymentMethodListParams{
		Customer: stripe.String(custID),
		Type:     stripe.String("card"),
	}
	i := paymentmethod.List(paymentMethodListParams)
	for i.Next() {
		pm := i.PaymentMethod()
		fmt.Printf("Payment card brand: %s\n", pm.Card.Brand)
	}
	return nil
}

func createTokenID(custID string) (string, error) {
	params := &stripe.TokenParams{
		Card: &stripe.CardParams{
			Number:   stripe.String("4242424242424242"),
			ExpMonth: stripe.String("03"),
			ExpYear:  stripe.String("2025"),
			CVC:      stripe.String("836"),
		},
	}
	token, err := token.New(params)
	if err != nil {
		return "", err
	}
	fmt.Printf("Token: %s\n", token.ID)
	return token.ID, nil
}

func createPaymentCard(custID string) error {
	tokenID, err := createTokenID(custID)
	if err != nil {
		return err
	}
	params := &stripe.CardParams{
		Customer: stripe.String(custID),
		Token:    stripe.String(tokenID),
	}
	card, err := card.New(params)
	if err != nil {
		return err
	}
	fmt.Printf("Card CVC check: %s\n", card.CVCCheck)
	return nil
}

func createTestCharge(custID string) error {
	tokenID, err := createTokenID(custID)
	if err != nil {
		return err
	}
	params := &stripe.ChargeParams{
		Amount:      stripe.Int64(1000),
		Currency:    stripe.String(string(stripe.CurrencyCAD)),
		Description: stripe.String("Subscription Charge"),
		Source:      &stripe.PaymentSourceSourceParams{Token: stripe.String(tokenID)},
	}
	charge, err := charge.New(params)
	if err != nil {
		return err
	}
	fmt.Printf("Charge Status: %s\n", charge.Status)
	return nil
}

func createPaymentLink(priceID string) error {
	params := &stripe.PaymentLinkParams{
		LineItems: []*stripe.PaymentLinkLineItemParams{
			{
				Price:    stripe.String(priceID),
				Quantity: stripe.Int64(1),
			},
		},
		SubscriptionData: &stripe.PaymentLinkSubscriptionDataParams{
			TrialPeriodDays: stripe.Int64(30),
		},
		AfterCompletion: &stripe.PaymentLinkAfterCompletionParams{
			Type: stripe.String("redirect"),
			Redirect: &stripe.PaymentLinkAfterCompletionRedirectParams{
				URL: stripe.String("https://docs.funnelish.com/start/collecting-payments/card-payments/stripe"),
			},
		},
	}
	paymentLink, err := paymentlink.New(params)
	if err != nil {
		return err
	}
	fmt.Printf("Payment link: %s\n", paymentLink.URL)
	fmt.Println("Enter any email address - payment confirmation email is disabled in test mode")
	fmt.Println("Use credentials from lines 74, 78, 141-144 in code above for credit card info")
	if err := openBrowser(paymentLink.URL); err != nil {
		return err
	}
	return nil
}

func openBrowser(url string) error {
	var err error
	switch runtime.GOOS {
	case "linux":
		err = exec.Command("xdg-open", url).Start()
	case "windows":
		err = exec.Command("rundll32", "url.dll,FileProtocolHandler", url).Start()
	case "darwin":
		err = exec.Command("open", url).Start()
	default:
		err = fmt.Errorf("unsupported platform")
	}
	if err != nil {
		return err
	}
	return nil
}
