module gitlab.com/geospatialweb/stripe-test

go 1.20

require (
	github.com/caarlos0/env/v8 v8.0.0
	github.com/joho/godotenv v1.5.1
	github.com/stripe/stripe-go/v74 v74.18.0
)
