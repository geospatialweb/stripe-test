package env

import (
	"fmt"
	"log"

	"github.com/caarlos0/env/v8"
	"github.com/joho/godotenv"
)

type Config struct {
	StripeSecretKey      string `env:"STRIPE_SECRET_KEY"`
}

func LoadEnv(envFile string) (Config, error) {
	cfg := Config{}
	if err := godotenv.Load(envFile); err != nil {
		return cfg, fmt.Errorf(".env load failure:\n%s", err)
	}
	opts := env.Options{RequiredIfNoDef: true}
	if err := env.ParseWithOptions(&cfg, opts); err != nil {
		return cfg, fmt.Errorf(".env parse failure:\n%s", err)
	}
	log.Println("\033[36m", ".ENV load success")
	return cfg, nil
}
