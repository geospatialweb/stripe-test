Go sample test runs:

- Go HTTP GET and POST requests located in `cmd/request` with Makefile

- Go Stripe payment processor simulation located in `cmd/stripe` with Makefile

To use your own Stripe Secret TEST Key, comment out lines 18, 22-26 in `stripe.go` and insert your own test key.
Otherwise, .env file is available upon request at johncampbell@geospatialweb.ca
